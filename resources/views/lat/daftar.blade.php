
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>SIGN UP</title>
</head>
<body>
    <h1>Buat Account Baru</h1>
    <h3>Sign Up Form</h3>
    <form action="/kirim" method="post">
        @csrf
        <label for="nama_awal ">First Name</label> <br>
        <input type="text" name="nama_awal" > <br>
        <label for="nama_akhir ">Last Name</label> <br>
        <input type="text" name="nama_akhir" > <br>
        <p>Gender :</p>        
        <input type="radio" name="gender" id="gender"> <label for="male">Male</label> <br>
        <input type="radio" name="gender" id="gender"> <label for="female">Female</label> <br>
        <input type="radio" name="gender" id="gender"> <label for="other">Other</label> <br>

        <label for="bangsa">Nationality :</label>
        <select name="bangsa" id="bangsa">
          <option value="indonesia">Indonesia</option>
          <option value="amerika">Amerika</option>
          <option value="inggris">Inggris</option>
          <option value="saudi">Arab Saudi</option>
        </select>
        <br>
        <p>Language Spoken :</p>        
        <input type="checkbox" name="language" id="language"> <label for="ind">Indonesia</label> <br>
        <input type="checkbox" name="language" id="language"> <label for="eng">English</label> <br>
        <input type="checkbox" name="language" id="language"> <label for="oth">Other</label> <br>
        <p>Bio</p>
        <textarea name="bio" id="" cols="30" rows="10"></textarea> <br>
        <input type="submit" value="Sign Up">
    </form> 
    
</body>
</html>
